import numpy as np

def montaMatrizTridiagonal(b=np.ndarray, c=np.ndarray, a=np.ndarray):
    n = len(b)
    # Verificação de Erros
    if a.ndim != 1:
        raise Exception('Erro: Diagonal inferiror deve ser um vetor nx1')
    if b.ndim != 1:
        raise Exception('Erro: Diagonal principal deve ser um vetor nx1')
    if c.ndim != 1:
        raise Exception('Erro: Diagonal superior deve ser um vetor nx1')
    if len(c) != n or len(a) != n:
        raise Exception('Uma ou mais das diagonais têm tamanho incorreto, reveja as diagonais')

    
    A = np.zeros((n,n))                                    # Cria matriz nxn nula

    for i in range(len(A)):                                # Varre cada linha da matriz vazia
        if i<len(b):
            A[i,i] = b[i]                  # Monta diagonal principal

        if i!=0 and i<len(c)+1:
            A[i-1, i] = c[i-1]              # Monta diagonal superior sem o ultimo elemento do vetor da diagonal superior
            
        if i<(len(A)-1) and i<len(a):
            A[i+1, i] = a[i+1]                # Monta diagonal inferior sem o primeiro elemento do vetro da diagonal inferior
    
    A[0,len(b)-1] = a[0]
    A[len(b)-1,0] = c[-1]

    return A

def tridiagonalParaVetores(A=np.ndarray):
    a = np.zeros(len(A))                  # Cria os vetores para guardar as diagonais
    b = np.copy(a)
    c = np.copy(a)

    for i in range(len(A)):                 # Varre cada linha da matriz
        if i<len(A):
            b[i] = A[i,i]                   # Guarda a diagonal principal

        if i!=0 and i<len(A):
            c[i-1] = A[i-1, i]              # Guarda a diagonal superior
            
        if i<(len(A)-1):
            a[i+1] = A[i+1, i]                # Guarda a diagonal inferior

    return b, c, a

def decompLU(Matriz=np.ndarray):
    b, c, a = tridiagonalParaVetores(Matriz)

    u = np.zeros(len(Matriz))
    l = np.zeros(len(Matriz)-1)
    
    u[0] = b[0]

    for i in range(len(Matriz)-1):
        if i<=len(Matriz)-2:
            l[i] = a[i+1]/u[i]
        u[i+1] = b[i+1]-l[i]*c[i]
  
    return l, u

def decompLUVet(b=np.ndarray, c=np.ndarray, a=np.ndarray ):
    u = np.zeros(len(b))
    l = np.zeros(len(b)-1)
    
    u[0] = b[0]

    for i in range(len(b)-1):
        if i<len(b)-1:
            l[i] = a[i+1]/u[i]
        u[i+1] = b[i+1]-l[i]*c[i]

    return l, u

def montaMatrizL(l=np.ndarray):
    L = np.eye(len(l)+1, len(l)+1)

    for i in range(len(L)-1):
        L[i+1, i] = l[i]

    return L

def montaMatrizU(u=np.ndarray, c=np.ndarray):
    U = np.zeros((len(u), len(u)))
    
    for i in range(len(u)):
        U[i,i] = u[i]
        
        if i<len(u)-1:
            U[i, i+1] = c[i]

    return U    

def solucionaSistemaL(l=np.ndarray, B=np.ndarray):
    y = np.zeros(len(l)+1)
    y[0] = B[0]

    for i in range(1, len(l)):
        y[i] = B[i]-(l[i-1]*y[i-1])

    y = np.reshape(y, (len(y),1))
    return y

def solucionaSistemaU(u=np.ndarray, c=np.ndarray, y=np.ndarray):
    x = np.zeros(len(u))
    x[-1] = y[-1]/u[-1]

    for i in range(len(u)-2, -1, -1):
        x[i] = (y[i]-c[i]*x[i+1])/u[i]
        
    x = np.reshape(x, (len(x),1))    

    return x

def solucionaSistemaTridiagonal(b=np.ndarray, c=np.ndarray, a=np.ndarray, d=np.ndarray):
   x = np.zeros(len(b))
   
   db = np.copy(d)
   db = np.delete(db, -1)

   v = np.zeros(len(b)-1)
   v[0] = a[0]
   v[-1] = c[-2]

   w = np.zeros(len(b)-1)
   w[0] = c[-1]
   w[-1] = a[-1]
   
   l, u = decompLUVet(b, c, a)

   y = solucionaSistemaL(l, db)
   yb = solucionaSistemaU(u, c, y)

   z = solucionaSistemaL(l, v)
   zb = solucionaSistemaU(u, c, z)

   x[-1] = (d[-1] - c[-1]*yb[0] - a[-1]*yb[-2])/(b[-1] - c[-1]*zb[0] - a[-1]*zb[-2])

   for i in range(len(x)-1):
      x[i] = yb[i] - x[-1]*zb[i]

   x = np.reshape(x, (len(x), 1))
   return(x)

def calculaResult(b, c, a, x):
    r = np.zeros(len(b))
    
    r[0] = a[0]*x[-1] + b[0]*x[0] + c[0]*x[1]
    r[-1] = a[-1]*x[-2] + b[-1]*x[-1] + c[-1]*x[0]

    for i in range(1,len(b)-1):
        r[i] = a[i]*x[i-1] + b[i]*x[i] + c[i]*x[i+1]

    return r

def residuoMax(b=np.ndarray, c=np.ndarray, a=np.ndarray, x=np.ndarray, d=np.ndarray):
    result = calculaResult(b, c, a, x)
    residMax = np.max(np.absolute(result - d))

    return residMax

def testeAleatorio(n):
    b = np.zeros(n)
    c = np.zeros(n)
    a = np.zeros(n)
    d = np.zeros(n)

    for i in range(n):
        b[i] = np.random.randn()
        a[i] = np.random.randn()
        c[i] = np.random.randn()
        d[i] = b[i]+c[i]+a[i]

    x = solucionaSistemaTridiagonal(b,c,a,d)
    residuo = residuoMax(b,c,a,x,d)
    
    print(f"O maior resíduo devido ao ponto flutuante computacional é de {residuo}")