import numpy as np

from functions import residuoMax, solucionaSistemaTridiagonal, testeAleatorio

def main():
                                        #----------------------------------------------------------------#
                                                        #Parametros para montar a Matriz    

    #Tamanho da matriz
    n = 20                    

    #vetores que armazenam as diagonais
    b = np.zeros(n)          
    c = np.copy(b)
    a = np.copy(b)
    d = np.copy(b)


    #Funções que geram os elementos dos vetores a, b e c 

    fa = lambda i : (2*i**2 - 1)/(n*i)  
    fb = lambda i : 2
    #fc = lambda i:                                           #o vetor c é montado com referência ao vetor a, mas é possível montá-lo de maneira similar aos outros se for o caso
    fd = lambda i : np.cos((2*np.pi*i**2)/(n**2))


    for i in range(n):

        if i<n-1:
            a[i] = fa(i+1)
        else:
            #escolhe o ultimo elemento do vetor a arbritariamente
            a[i] = fa(i+1)*2           

        b[i] = fb(i+1)
        c[i] = 1-a[i]
        d[i] = fd(i+1)

                                        #----------------------------------------------------------------#

    print("Escolha um modo: ")
    print("1: Solucionar matriz escolhida pelo usuário")
    print("2: Solucionar matriz aleatória")
    modo = input("Modo: ")

    if modo == "1":
        #resolve o sistema e calcula o resíduo
        x = solucionaSistemaTridiagonal(b,c,a,d)
        residuo = residuoMax(b,c,a,x,d)

        print("A solução da matriz montada é: ")
        for i in range(1,n+1):
            print(f"x{i} = {x[i-1]}" )
        
        print(f"O maior resíduo devido ao ponto flutuante computacional é de {residuo}")
    
    elif modo == "2":
        n = input("Tamanho da matriz aleatória: ")
        testeAleatorio(int(n))

    else:
        raise Exception("Modo inválido")

if __name__ == "__main__":
    main()