# -- Versões -- #
Progrmado em: Python 3.8.0
Bicliotecas utilizadas: Numpy 1.21.1


# -- Instruções de utilização do programa -- #

Ao rodar a função 'main' o usuário deve escolher um dos dois modos do programa; utilizar uma matriz gerada aleatóriamente ou uma matriz que foi gerada pelo usuário. 

## -- Matriz Aleatória -- ##
Ao selecionar o modo da matriz aletória, será pedido o tamanho da matriz que ele deseja testar. Após isso, o programa resíduo máximo, mas não a solução, uma vez que esse modo é pretendido para matrizes maiores, custando muito tempo para escrever no terminal todos os valores de cada incógnita.

## -- Matriz Escolhida -- ##
Selecionando o modo com a matriz costumizada, o usuário deve ter alterado o código de antemão. Na função 'main' estão as funções para a criação dos vetores que definem a matriz assim como seu tamanho. A função lambda fa define os elementos do vetor a (diagonal inferior), fb do vetor b (diagonal principal), fc do vetor c (diagonal superior) e fd do vetor d (vetor dos coeficientes independentes). Ainda é possivel definir os elementos de um dos vetores em relação aos outros, para isso basta colocar a condição (como por exemplo c[i] = 1-a[i]), no 'loop for' que monta as matrizes; note que é importante lembrar de comentar ou excluir a função geratriz do vetor (fa, fb,fc ou fd) e colocar o vetor que será montado a partir dos outros por ultimo no loop. Caso tenha um elemento específico de um dos vetores que não siga a função geratriz é possivel criar uma condição dentro do loop, de modo que no elemento desejado outra regra seja seguida.